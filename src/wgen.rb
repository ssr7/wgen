#!/usr/bin/env ruby
$name = "Seabreeze"
$source = "https://codeberg.org/shrike/wgen"
$input_dir = "inc"
$output_dir = "../site"
$static = "../static" # stylesheet, favicon, etc

def parse(f)
  content = File.read("#$input_dir/#{f}")
  tags = content.scan(/\{\/?[^\/].*?\}/)
  tags.each do |tag|
    filename, original = tag.tr("{/}", "")
    filename.gsub!(/\s/, "_")
    linkname = filename
    filename = "#{filename.downcase}.html"

    if tag.match?(/\{\/.*?\}/) # for embeds {/file}
      if File.exist?("#$input_dir/#{filename}")
        content.sub!(/#{tag}/,
          "<h2>#{linkname.gsub(/\w+/, &:capitalize)}</h2>\n" +
          File.read("#$input_dir/#{filename}"))
      else
        puts "Missing file: #{filename} embedded on #{f}"
        content.sub!(/#{tag}/, "<div class='error'>Missing file: #{filename}</div>")
      end
    else
      a_tag = tag.sub(/^./, "<a href='#{filename}'>")
      a_tag.sub!(/.$/, "</a>")
      content.sub!(/#{Regexp.escape(tag)}/, a_tag)
    end
  end

  return content
end

def build()
  files = Dir.children("inc").sort
  files.keep_if {|file| file =~ /^[^.]+\.html$/}

  if files.empty?
    abort("No files found in .\/#$input_dir")
  end

  puts "Files found: #{files.inspect}"

  i = 0
  files.each do |file|
    File.open("#$output_dir/#{file}", "w") do |f|
      title = File.basename(file, ".html").capitalize()
      nav = parse("meta.nav.html")
      content = parse(file)
      modified = File.mtime("#$input_dir/#{file}")

      if file == "index.html"
        content = "<ul>"
        files.each do |file|
          content += "<li><a href='#{file}' class='cap'>#{file.split(/\s|\./)[0]}</a></li>\n"
        end
        content += "</ul>"
      end

      f.puts "<!DOCTYPE html><html lang='en'>"
      f.puts "<head><meta charset='utf-8'>"
      f.puts "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"
      f.puts "<title>#$name - #{title}</title>"
      f.puts "<link href='#$static/style.css' rel='stylesheet'>"
      f.puts "<link href='#$static/icon.svg' rel='icon'>"
      f.puts "</head><body>"
      f.puts "<header><a href='home.html'><img src='#$static/icon.svg'><span>#$name</span></a></header>"
      f.puts "<nav><details open><summary>Menu</summary><div class='nav_links'>#{nav}</div></details></nav>"
      f.puts "<main><h1>#{title}</h1>"
      f.puts "#{content}"
      f.puts "</main>"
      f.puts "<footer>"
      f.puts "<span>Last modified: #{modified.strftime("%T %Z %A, %-d %B %Y")}"
      f.puts "<a href='#$source/_edit/master/src/inc/#{file}' target='_blank'>[edit]</a></span>"
      f.puts "</footer>"
      f.puts "</body></html>"
      f.close
      puts "Generated: #{file}"
    end

    i += 1
  end

  puts "#{i} files generated"
end

build()
